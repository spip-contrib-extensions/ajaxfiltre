# Ajaxfiltre

Ce plugin permet de créer une navigation par facettes qui recharge instantanément une liste d’objets en ajax dès qu'on modifie une valeur du formulaire (un champ input, un select, une case à cocher...).

Fonctionne avec n'importe quel squelette, ou liste d'objets, il suffit de préciser l'idnetifiant lors de l'inclusion ajax (par défaut `liste-objets`) dans le squelette appelant.

Voir la page de démo (/ecrire/?exec=ajaxfiltre_articles) et ces trois squelettes documentés :
* [ajaxfiltre/prive/objets/liste/ajaxfiltre_articles.html](https://git.spip.net/spip-contrib-extensions/ajaxfiltre/src/branch/readme/prive/objets/liste/ajaxfiltre_articles.html)
* [ajaxfiltre/prive/squelettes/contenu/ajaxfiltre_articles.html](https://git.spip.net/spip-contrib-extensions/ajaxfiltre/src/branch/readme/prive/squelettes/contenu/ajaxfiltre_articles.html)
* [ajaxfiltre/prive/squelettes/navigation/ajaxfiltre_articles.html](https://git.spip.net/spip-contrib-extensions/ajaxfiltre/src/branch/readme/prive/squelettes/navigation/ajaxfiltre_articles.html)

Fonctionne aussi très bien côté public.

Il suffit d'ajouter le chargement de ajaxfiltre dans `inclure/head.html` :

```html 
[<script src="(#CHEMIN{prive/javascript/ajaxfiltre_prive.js}|timestamp)"></script>]
```

Et de créer des formulaires CVT de ce type

`formulaires/recherche.html`

```html 
<div class="formulaire_spip"  id="formulaire_#FORM">
	<form method='post' action="#SELF#resultats-recherche">
		<div class="editer-groupe">
			#SAISIE{input, recherche, label=Recherche libre}
			#SAISIE{selection, bidule, label=Bidule, data=#ARRAY{...}}
			#SAISIE{case, dacopdac, label=D'ac ou pas d'ac}
			(...)
		</div>
		<div class="boutons">
			<button type="submit" name="valide">Recherche</button>
		</div>
	</form>
</div>

[(#REM) On précise bien l'id du bloc ajax : liste-objets, sinon utiliser les options de ajaxFiltre ]
<INCLURE{fond=formulaires/inc-recherche-resultats,env,ajax=liste-objets}>

[(#REM) On déclenche ajaxfiltre sur le formulaire ]
<script>
  $(function() {
    if('function' === typeof $.ajaxFiltre) {
      $('#formulaire_#FORM').ajaxFiltre();
    }
  });
</script>
```

NB : fonctionne aussi en ciblant le ou les formulaires eux même, exemple : $('form').ajaxFiltre();
