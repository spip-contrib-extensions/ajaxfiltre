(function($) {

	$.ajaxFiltre = function(el, options) {
		const base = this;
		const ignoreKeys = [];

		base.init = function() {
			base.$el = $(el).is('form') ? $(el) : $(el).find('form:first');
			if(!base.$el.length) {
				return;
			}
			base.options = $.extend({}, $.ajaxFiltre.defaultOptions, options);

			base.$el.on('change', 'select, input[type="checkbox"], input[type="radio"]', function() {
				base.query();
			});
			base.$el.on('focus', 'input:not([type="checkbox"]):not([type="radio"])', function() {
				$(this).data('initValue', $(this).val());
			});
			base.$el.on('blur', 'input:not([type="checkbox"]):not([type="radio"])', function() {
				if($(this).data('initValue') !== $(this).val()){
					base.query();
				}
			});

			base.ignoreKeys = [
				'var_ajax',
				'formulaire_action',
				'formulaire_action_args',
				'formulaire_action_sign',
			];

			base.$el.on('submit', function(e) {
				base.query();
				e.preventDefault();
			});

			base.checkInitState();
		};

		base.getFormData = function() {
			// lire les valeurs du formulaire
			let formData = base.$el.serializeArray().reduce(function(obj, item) {
				// si le name comporte des [] on construit un tableau
				if(item.name.indexOf('[]') !== -1) {
					const name = item.name.replace(/[\[\]]+/g, '');
					if(!obj.hasOwnProperty(name)) {
						obj[name] = [];
					}
					obj[name].push(item.value);
				} else {
					obj[item.name] = item.value;
				}
				return obj;
			}, {});

			// ignorer certaines valeurs
			for(const key in formData) {
				if(base.ignoreKeys.includes(key)) {
					delete formData[key];
				}
			}
			
			// reset pagination (comportement par défaut)
			if(base.options.resetPagination) {
				const preg =  /^debut_\w+/;
				const params = new URLSearchParams(window.location.search);
				params.forEach((value, key) => {
					if(key.match(preg)) {
						formData[key] = '';
					}
				});
			}

			// passer une valeur vide explicite pour les checkbox dont le name comporte des [] 
			// et dans lesquels rien n'est coché
			const $radios = base.$el.find('input[type=checkbox][name*="[]"]');
			$.each($radios, function() {
				const radioName = $(this).attr('name');
				// si rien n'est coché
				if(!base.$el.find('input[name="' + radioName + '"]:checked').length) {
					const name = radioName.replace(/[\[\]]+/g, '');
					// et si on n'a pas déjà des données
					if(!formData[name] || !formData[name].length) {
						// supprimer les données du nom avec []
						delete formData[radioName];
						// ajouter un tableau vide sur le nom sans [] 
						formData[name] = [];
					}
				}
			});
			
			// passer une valeur vide explicite pour les select sans [] et sans valeur choisie
			const $selects = base.$el.find('select:not([name*="[]"])');
			$.each($selects, function() {
				const selectName = $(this).attr('name');
				// si rien n'est coché
				if(!base.$el.find('select[name="' + selectName + '"] > options:selected').length) {
					// et si on n'a pas déjà des données
					if(!formData[selectName]) {
						// ajouter une chaine vide 
						formData[selectName] = '';
					} 
				}
			});
			
			return formData;
		}

		base.checkInitState = function() {
			let needUpdate = false;
			const formData = base.getFormData();

			// ignorer les valeurs des champs hidden
			base.$el.find('input[type="hidden"]').each(function() {
				delete formData[$(this).attr('name')];
			});

			for(const key in formData) {
				if(formData[key].length && !(formData[key] instanceof Array && !formData[key].some(Boolean))) {
					needUpdate = true;
				}
			}
			if(needUpdate) {
				base.query();
			}
		}

		base.query = function() {
			// recharger la liste d'objets
			ajaxReload(base.options.ajaxTarget, {args: base.getFormData()});
		};

		base.init();
	};

	$.ajaxFiltre.defaultOptions = {
		// cible ajax à recharger
		ajaxTarget: "liste-objets",
		// par défaut, supprimer la pagination lors d'un rechargement ajax
		resetPagination: true,
	};

	$.fn.ajaxFiltre = function(options) {
		return this.each(function() {
			(new $.ajaxFiltre(this, options));
		});
	};

})(jQuery);

$(function() {
	$('.formulaire_navigation_filtre').ajaxFiltre();
});